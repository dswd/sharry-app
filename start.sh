#!/bin/bash

set -eu -o pipefail

LIMIT=$(($(cat /sys/fs/cgroup/memory/memory.memsw.limit_in_bytes)/2**20))
if [ "$LIMIT" -gt 2048 ]; then
  LIMIT=2048
fi

if ! [ -e /app/data/.salt ]; then
  dd if=/dev/urandom bs=1 count=1024 2>/dev/null | sha1sum | awk '{ print $1 }' > /app/data/.salt
fi
SALT=$(cat /app/data/.salt)

export SHARRY_JAVA_OPTS="-XX:MaxRAM=${LIMIT}M -Dsharry.smtp.host=${MAIL_SMTP_SERVER} -Dsharry.smtp.port=${MAIL_SMTP_PORT} -Dsharry.smtp.user=${MAIL_SMTP_USERNAME} -Dsharry.smtp.password=${MAIL_SMTP_PASSWORD} -Dsharry.smtp.from=${MAIL_FROM} -Dsharry.web.baseurl=${APP_ORIGIN}/ -Dsharry.authc.app-key=hex:${SALT}"

exec /app/code/sharry.sh /app/code/application.conf
