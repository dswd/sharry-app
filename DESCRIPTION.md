Sharry allows to share files with others in a simple way. It is a self-hosted web application. The basic concept is: upload files and get a url back that can then be shared.

This app packages Sharry <upstream>0.2.0</upstream>


### How it works

#### Authenticated users -&gt; others

Authenticated users can upload their files on a web site together with
an optional password and a time period. The time period defines how long
the file is available for download. Then a public URL is generated that
can be shared, e.g. via email.

The download page is hard to guess, but open to everyone.


#### Others -&gt; Authenticated users

Anonymous can send files to registered ones. Each registered user can
maintain alias pages. An alias page is behind a “hard-to-guess” URL
(just like the download page) and allows everyone to upload files to the
corresponding user. The form does not allow to specify a password or
validation period, but a description can be given. The user belonging to
the alias can be notified via email.


#### Others -&gt; Others

If authentication is enabled, it is not possible to share files between
non-registered users. One party must be registered. But authentication
can be completely disabled. Then any user can upload files. This may be
useful within a closed network.


### Upload and Download

Sharry aims to provide a good support for large files. That means
downloads and uploads are resumable. Large files can be downloaded via
[byte serving](https://en.wikipedia.org/wiki/Byte_serving), which allows
for example to watch video files directly in the browser. Uploads are
resumable, too, by using
[resumable.js](https://github.com/23/resumable.js) on the client.
Uploads can be retried where only chunks not already at the server are
transferred.

Each published upload has a validity period, after which the public
download page doesn't work anymore. A cleanup job running periodically
can delete those files to save space.


### Features

-   resumable and recoverable upload of multiple files; thanks to
    [resumable.js](https://github.com/23/resumable.js)
-   validation period for uploads
-   resumable downloads using [byte
    serving](https://en.wikipedia.org/wiki/Byte_serving)
-   download single files or all in a zip
-   protect downloads with a password
-   automatic removal of invalid uploads
-   external authentication (via system command or http requests)
-   managing accounts, uploads and alias pages


### Bug reports

Open bugs on [Github](https://git.cloudron.io/dswd/sharry-app/issues)
