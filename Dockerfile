FROM cloudron/base:0.10.0

ENV VERSION 0.2.0

RUN apt-get update \
    && apt-get install -y openjdk-8-jre \
    && rm -r /var/cache/apt /var/lib/apt/lists

RUN mkdir -p /app/code \
    && curl -L https://eknet.org/main/projects/sharry/sharry-server-0.2.0.jar.sh -o /app/code/sharry.sh \
    && chmod +x /app/code/sharry.sh

ADD start.sh /app/code/start.sh
ADD application.conf /app/code/application.conf
ADD ldap-auth /app/code/ldap-auth

WORKDIR /tmp

CMD [ "/app/code/start.sh" ]

